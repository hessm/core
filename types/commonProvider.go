package types

import "context"

type CryptoContext struct {
	Namespace string
	Context   context.Context
}

type CryptoIdentifier struct {
	KeyId         string
	CryptoContext CryptoContext
}

type CryptoKeyParameter struct {
	Identifier CryptoIdentifier
	KeyType    KeyType
}

type CryptoHashParameter struct {
	Identifier    CryptoIdentifier
	HashAlgorithm HashAlgorithm
}

type CryptoKey struct {
	Key     []byte //pem format expected
	Version string
	CryptoKeyParameter
}

type CryptoKeySet struct {
	Keys []CryptoKey
}

type CryptoProviderModule interface {
	GetCryptoProvider() CryptoProvider
}

type CryptoProvider interface {
	GetNamespaces(context context.Context) ([]string, error)
	GenerateRandom(context CryptoContext, number int) ([]byte, error)
	Hash(parameter CryptoHashParameter, msg []byte) ([]byte, error)
	Encrypt(parameter CryptoIdentifier, data []byte) ([]byte, error)
	Decrypt(parameter CryptoIdentifier, data []byte) ([]byte, error)
	Sign(parameter CryptoIdentifier, data []byte) ([]byte, error)
	GetKeys(parameter CryptoIdentifier) (CryptoKeySet, error)
	GetKey(parameter CryptoIdentifier) (CryptoKey, error)
	Verify(parameter CryptoIdentifier, data []byte, signature []byte) (bool, error)
	GenerateKey(parameter CryptoKeyParameter) error
	GetSeed(context context.Context) string
}
