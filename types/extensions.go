package types

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/lestrrat-go/jwx/v2/jwk"
)

func (key CryptoKey) GetPem() (p *pem.Block, err error) {
	if !strings.Contains(string(key.KeyType), "aes") {

		alg := "RSA"
		if strings.Contains(string(key.KeyType), "ec") {
			alg = "ECDSA"
		}

		if strings.Contains(string(key.KeyType), "ed") {
			alg = "EDDSA"
		}

		pub, err := jwt.ParseRSAPublicKeyFromPEM(key.Key)
		if err == nil {
			pubPEM := &pem.Block{
				Type:  alg + " PUBLIC KEY",
				Bytes: x509.MarshalPKCS1PublicKey(pub),
			}
			return pubPEM, err
		}

	}
	return nil, errors.New("Unsupported")
}

func (key CryptoKey) GetJwk() (jwk.Key, error) {
	jwk, err := jwk.ParseKey([]byte(key.Key), jwk.WithPEM(true))
	if err != nil {
		return nil, err
	}
	return jwk, nil
}
