package tests

import (
	"encoding/base64"
	"encoding/json"
	"os"
	"testing"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
)

const pem = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuPsD5or7uGVyy9WmTc6a
mWzpGIZzsKCceUOh2slnptD8W8od1unUMws3uFZAGSYDaBceSQ7Wy5i8IJYJAY9Z
u/GYGPMr3rfhzc4E1XVmuqhSO8QdrscnLxjn+dIWrUmzFXAnUKFaY0tMH6mrZug3
RNNKHSrbs1bisZrsqZXGM0vTEGyL3sxjwd7gi4DM7Y7Xvv9qcdDTEpZ7t14QfucN
l6V1FuVaNGwzst4Be9KDCNRTywIJ/Uogyy8OW9pKCVBpPJP9e/O607hAEgCE9nEG
ffnnZEVzs5QNu/PagUuZJABzsWZ4q++p8CVbzj1gED7DmLMNnUOxzlZ90ewFvDrd
cwIDAQAB
-----END PUBLIC KEY-----`

var pemAsJwk = map[string]string{
	"kty": "RSA",
	"e":   "AQAB",
	"use": "sig",
	"alg": "PS256",
	"n":   "uPsD5or7uGVyy9WmTc6amWzpGIZzsKCceUOh2slnptD8W8od1unUMws3uFZAGSYDaBceSQ7Wy5i8IJYJAY9Zu_GYGPMr3rfhzc4E1XVmuqhSO8QdrscnLxjn-dIWrUmzFXAnUKFaY0tMH6mrZug3RNNKHSrbs1bisZrsqZXGM0vTEGyL3sxjwd7gi4DM7Y7Xvv9qcdDTEpZ7t14QfucNl6V1FuVaNGwzst4Be9KDCNRTywIJ_Uogyy8OW9pKCVBpPJP9e_O607hAEgCE9nEGffnnZEVzs5QNu_PagUuZJABzsWZ4q--p8CVbzj1gED7DmLMNnUOxzlZ90ewFvDrdcw",
}

func TestExtensionJWK(t *testing.T) {
	key := types.CryptoKey{
		Key: []byte(pem),
	}

	jwk, err := key.GetJwk()

	if err != nil {
		t.Error()
	}

	json.NewEncoder(os.Stdout).Encode(jwk)

	p, b := jwk.Get("n")

	if !b {
		t.Error()
	}
	str := base64.RawURLEncoding.EncodeToString(p.([]byte))

	if str != pemAsJwk["n"] {
		t.Error()
	}
}

func TestExtensionJWKNilKey(t *testing.T) {
	key := types.CryptoKey{
		Key: nil,
	}

	jwk, err := key.GetJwk()

	if err == nil || jwk != nil {
		t.Error()
	}
}
