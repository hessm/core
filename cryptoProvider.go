package core

import (
	"plugin"

	"github.com/spf13/viper"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
)

var provider types.CryptoProvider

func init() {
	p, err := plugin.Open(viper.GetString("CRYPTO_MODULE_PATH"))
	if err != nil {
		panic(err)
	}

	v, err := p.Lookup("Plugin")
	if err != nil {
		panic(err)
	}
	provider = v.(types.CryptoProviderModule).GetCryptoProvider()
}

func CryptoEngine() types.CryptoProvider {
	return provider
}
